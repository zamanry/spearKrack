Start-Transcript -Path '.\.spearKrack.log' -Append -IncludeInvocationHeader -NoClobber
Set-StrictMode -Version 'Latest'

Write-Host ''
Write-Host "                                __                   __  "
Write-Host "    _________  ___  ____ ______/ /___________ ______/ /__"
Write-Host "   / ___/ __ \/ _ \/ __ }/ ___/ //_/ ___/ __ }/ ___/ //_/"
Write-Host "  (__  ) /_/ /  __/ /_/ / /  / .< / /  / /_/ / /__/ .<   "
Write-Host " /____/ .___/\___/\__,_/_/  /_/|_/_/   \__,_/\___/_/|_|  "
Write-Host "     /_/                                                 "
Write-Host 'Generate targeted authentication credentials through answering questions.'
Write-Host ''
Write-Host 'If one question has multiple answers, insert them all on the same line with spaces.'
Write-Host '		- Ex. What is their favorite color?: white black gray'
Write-Host ''
Write-Host "With that out of the way, let's crack some credentials."

Function Get-TargetType {
	Do {
		Write-Host ""
		Set-Variable -Name 'UserResponse' -Value (Read-Host -Prompt 'Is the entity an organization? (Y/n)')
	} Until (($UserResponse -eq 'Y') -or ($UserResponse -eq 'y') -or ($UserResponse -eq 'N') -or ($UserResponse -eq 'n'))
	If (($UserResponse -eq 'Y') -or ($UserResponse -eq 'y'))
		Set-Variable -Name 'Organization' -Value "$TRUE"
	Else {
		Set-Variable -Name 'Organization' -Value "$FALSE"
	}
}

[Bool]$Organization
Get-TargetType

[Array]$Questions
Set-Variable -Name 'Questions' -Value (Import-Csv -Path '.\spearKrack.ps1' -Delimiter ',')

If ($Organization -eq "$TRUE") {

}

[Array]$Topics =
	'LegacyComponents',
	'DirectPlay',

$ctr = '0'
Foreach ($Topic in $Topics) {
	Write-Host ""
	if ($Topic -eq 'City') {
		Write-Warning 'The following questions may have multiple answers including past homes, current homes, '
	}
	Set-Variable -Name "$ctr" -Value (Read-Host -Prompt "Does their building have a nickname? Ex. The White House")
	$ctr++
	}

Stop-Transcript
