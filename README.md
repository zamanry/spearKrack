# spearKrack
Generate targeted authentication credentials through answering questions.

## Features
- Username creation
- Passwords/passphrases creation
- PINs creation
- Supports credential length restrictions
- Line delimited text file output
